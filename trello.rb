# https://trello.com/b/1CCVefr5.json

require 'json'
require 'pp'
require 'net/http'
require 'date'
require 'open-uri'

BASE_URL = ARGV[0]

uri = URI(BASE_URL)
res = Net::HTTP.get(uri)
trello = JSON.parse(res)



title = trello["name"]

puts "Found a Trello board called " + title

puts "Found the following issue boards and issues"

trello["lists"].each do | child |
  puts child["name"]

  listid = child["id"]

  trello["cards"].each do | issues |

    if issues["idList"] == listid
      puts "\t" + issues["name"]
    end
end
end
